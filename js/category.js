// category-slider
$("#owl-category").owlCarousel({
    margin: 15,
    loop: true,
    callbacks: true,
    items: 4,
    slideSpeed: 200,
    autoplay: true,
    nav: false,
    autoPlaySpeed: 200,
    smartSpeed: 200,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
        },
        768: {
            items: 2,
        },
        992: {
            items: 3,
        },
        1250: {
            items: 4,
            loop: true
        }
    }
});

$('.owl-stage').change(function(){
    alert('asd');
});