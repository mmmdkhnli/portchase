$(document).ready(function(){
    let sh_login_click=true;

    $('.inner>form>label>.show-hide').click(()=>{
        if(sh_login_click==true){
            $('.inner>form>label>.show-hide>.round').css('float','right');
            sh_login_click=false;
            $('.inner>form>.password').attr('type','text');
        }else{
            $('.inner>form>label>.show-hide>.round').css('float','left');
            sh_login_click=true;
            $('.inner>form>.password').attr('type','password');
        }
    });
});