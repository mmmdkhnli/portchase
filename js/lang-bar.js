let a = false;

$('.lang-bar').on('click',function(){
    if(a == false){
        $('.lang-bar>.inner').show().css('z-index','4');
        a = true;
    }else{
        $('.lang-bar>.inner').hide();
        a = false;
    }
});

$(document).click(function(e) {
    var target = e.target;

    if (!$(target).is('.lang-bar') && !$(target).parents().is('.lang-bar')) {
        $('.lang-bar>.inner').hide();
        a = false;
    }
});