$('.shopping-cart .remove-item').click(function(){
    swal({
        title: "Əminsən?",
        text: "Seçdiyiniz Məhsul Səbətdən Silinəcək",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sil!",
        cancelButtonText: "Geri",
        closeOnConfirm: false
      },
      function(){
        swal("Silindi!", "Məhsul Səbətdən Uğurla Silindi.", "success");
      });
});