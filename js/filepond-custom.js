//image upload

// Register the plugin
FilePond.registerPlugin(FilePondPluginImagePreview);

// Turn input element into a pond registerPlugin(FilePondPluginImagePreview)
$('.my-pond').filepond();

// Set allowMultiple property to true
$('.my-pond').filepond('allowMultiple', false);

// Set allowMultiple property to true
$('.my-pond').filepond('allowImagePreview', true);

// Listen for addfile event
$('.my-pond').on('FilePond:addfile', function (e) {
    console.log('file added event', e);
});