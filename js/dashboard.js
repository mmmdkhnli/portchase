// dashboard-manage
$('.dashboard-inner>.manage-panel>ul>li').on('click',function(){
    $(this).find('ul').slideToggle(200); 
    $(this).find('.fa-caret-down').toggleClass('.fas fa-caret-up');
});

// file-upload
$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

// sub-c

$('._product-sub-category').hide();

$("._product-category select").change(function() {
    if($('._product-category select option:selected').val() != 'none'){
        $('._product-category').css('width','48%').css('transition','0.2s');
        $('._product-sub-category').slideDown(200).css('width','48%')
    }
});

