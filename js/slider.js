// products
$(".last-blogs .owl-carousel").owlCarousel({
    margin: 15,
    loop: true,
    callbacks: true,
    items: 4,
    slideSpeed: 200,
    autoplay: true,
    nav: false,
    autoPlaySpeed: 200,
    smartSpeed: 200,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
        },
        700: {
            items: 2,
        },
        992: {
            items: 2,
        },
        1250: {
            items: 2,
            loop: true
        }
    }
});


// blogs

$("#owl-carousel2").owlCarousel({
    margin: 15,
    loop: true,
    callbacks: true,
    items: 4,
    slideSpeed: 200,
    autoplay: true,
    nav: false,
    autoPlaySpeed: 200,
    smartSpeed: 200,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
        },
        768: {
            items: 2,
        },
        992: {
            items: 3,
        },
        1250: {
            items: 4,
            loop: true
        }
    }
});

//main

$('#owl-main').owlCarousel({
    margin: 15,
    loop: true,
    callbacks: true,
    items: 4,
    slideSpeed: 200,
    autoplay: true,
    nav: false,
    autoPlaySpeed: 200,
    smartSpeed: 200,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
        },
        768: {
            items: 1,
        },
        992: {
            items: 1,
        },
        1250: {
            items: 1,
            loop: true
        }
    }
})

// product-slider-horizontal
$("#product-slider-horizontal").owlCarousel({
    margin: 15,
    loop: true,
    callbacks: true,
    items: 4,
    slideSpeed: 200,
    autoplay: true,
    nav: false,
    autoPlaySpeed: 200,
    smartSpeed: 200,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
        },
        375: {
            items: 2,
        },
        560: {
            items: 3,
        },
        992: {
            items: 4,
        },
        1250: {
            items: 4,
            loop: true
        }
    }
});