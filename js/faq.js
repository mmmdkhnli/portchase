$('.card').on('show.bs.collapse', function () {
    $(this).find('i').removeClass('fa-angle-right').addClass('fa-angle-down');
  });

  $('.card').on('hide.bs.collapse', function () {
    $(this).find('i').removeClass('fa-angle-down').addClass('fa-angle-right');
  });