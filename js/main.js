$(document).scroll(function(){
    if(document.documentElement.scrollTop>40){
        $('.navbar').css('position','fixed').css('top','0').css('left','0');
        $('.top-menu').css('margin-bottom','75px');
    }else if(document.documentElement.scrollTop<=40){
        $('.navbar').css('position','relative');
        $('.top-menu').css('margin-bottom','0');
    }
})


// add-basket
let count=0;

$('.add-bucket').on('click',function(){

    count++;

    $('.icofont-shopping-cart>.count').show(0).css('display','block');
    $('.icofont-shopping-cart>.count').text(count);
    if(count>99){
        $('.icofont-shopping-cart>.count').text("99+");
    }

    $(this).hide(0);
    $(this).siblings('.remove-bucket').show(0).css('display','flex');

});

$('.remove-bucket').on('click',function(){
    count--;

    if(count<=0){
        $('.icofont-shopping-cart>.count').show(0).css('display','none');
        count=0;
    }else if(count>0){
        $('.icofont-shopping-cart>.count').text(count);
        if(count>99){
            $('.icofont-shopping-cart>.count').text("99+");
        }
    }

    $(this).hide(0);
    $(this).siblings('.add-bucket').show(0).css('display','flex');
});


// m-navigation
$('.m-navigation>ul>li').on('click',function(){
    $(this).find('ul').slideToggle(200); 
    $(this).find('.fa-caret-down').toggleClass('.fas fa-caret-up');
});


$('.open-menu').on('click',function(){
    $('.m-navigation').css('margin-left','0').css('transition','0.35s');
});

$('.close-nav').on('click',function(){
    $('.m-navigation').css('margin-left','-250vw').css('transition','0.35s');
});

