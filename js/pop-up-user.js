let b = false;

$('.tools .pop-up').on('click',function(){
    if(b == false){
        $('.navbar .tools ul').show();
        b = true;
    }else{
        $('.tools ul').hide();
        b = false;
    }
});

$(document).click(function(e) {
    var target = e.target;

    if (!$(target).is('.tools .pop-up') && !$(target).parents().is('.tools .pop-up')) {
        $('.navbar .tools ul').hide();
        b = false;
    }
});