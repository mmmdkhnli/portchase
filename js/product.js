// slider
$('#lightSlider').lightSlider({
    gallery: true,
    item: 1,
    loop:true,
    slideMargin: 0,
    thumbItem: 9
});

// product-description
$('.product-header .product-description #tabs>li>a').on('click',function(){
    $('.product-header .product-description #tabs>li>a').css('color','#666666');
    $(this).css('color','#E95959');
});

$('.product-header .product-description #productTabs>li').on('click',function(){
    $('.product-header .product-description #productTabs>li>a').css('color','#666666');
    $(this).find('a').css('color','#e95959');
});

$('.product-header .product-description #companyTabs>li').on('click',function(){
    $('.product-header .product-description #companyTabs>li>a').css('color','#666666');
    $(this).find('a').css('color','#e95959');
});


let countItems=$('.product-header>.product-header-inner>.other>.other-products>.last-product-slider>.items').find('.item').length;
let interval=countItems-4;
let sliderItemHeight=$('.product-header>.product-header-inner>.other>.other-products>.last-product-slider>.items').find('.item').outerHeight();

let marginItem=0;
$('.product-header>.product-header-inner>.other>.other-products>.last-product-slider').find('.next-button').on('click',function(){
    marginItem+=sliderItemHeight+5;
    if(marginItem>=(interval+1)*sliderItemHeight){
        marginItem=0;
    }
    $('.product-header>.product-header-inner>.other>.other-products>.last-product-slider>.items>.item:first-child').css('margin-top','-'+marginItem+'px').css('transition','0.4s')
});

$('.product-header>.product-header-inner>.other>.other-products>.last-product-slider').find('.prev-button').on('click',function(){
    marginItem-=sliderItemHeight+5;
    if(marginItem<0){
        marginItem=interval*(sliderItemHeight+5);
    }
    $('.product-header>.product-header-inner>.other>.other-products>.last-product-slider>.items>.item:first-child').css('margin-top','-'+marginItem+'px').css('transition','0.4s')
});

let myInterval = setInterval(() => {
    marginItem+=sliderItemHeight+5;
    if(marginItem>=(interval+1)*sliderItemHeight){
        marginItem=0;
    }
    $('.product-header>.product-header-inner>.other>.other-products>.last-product-slider>.items>.item:first-child').css('margin-top','-'+marginItem+'px').css('transition','0.4s')
}, 2000);
