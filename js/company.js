$('.open-filter').on('click',function(){
    $('.mobile-filter').css('margin-left','0').css('transition','0.2s');
    $('.mobile-filter-cover').fadeIn(200);
});

$('.mobile-filter-cover').on('click',function(){
    $('.mobile-filter').css('margin-left','-100vw').css('transition','0.2s');
    $(this).fadeOut(200);
});

$('.close-filter').on('click',function(){
    $('.mobile-filter').css('margin-left','-100vw').css('transition','0.2s');
    $('.mobile-filter-cover').fadeOut(200);
});