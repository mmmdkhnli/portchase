$(document).ready(function(){
    $(document).delegate('.add_item>.inner', 'click', function(e) {
        e.preventDefault();    
        var content = $('#sample_table tr'),
        size = $('#my_table >tbody >tr').length + 1,
        element = null,    
        element = content.clone();
        element.attr('id', 'rec-'+size);
        element.find('.delete-record').attr('data-id', size);
        element.appendTo('#tbl_posts_body');
        element.find('.sn').html(size);
    });
    
    $(document).delegate('.delete-record', 'click', function(e) {
        e.preventDefault();    
        var didConfirm = confirm("Bu sətirin silinməsini istəyirsiniz?");
        if (didConfirm == true) {
        var id = $(this).attr('data-id');
        $('#rec-' + id).remove();
    
        $('#tbl_posts_body tr').each(function(index){
            $(this).find('span.sn').html(index+1);
        });
    
        return true;
        } else {
            return false;
        }
    });
    
    
    // -------------
    function exportTableToExcel(tableID, filename){
        var downloadLink;
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById(tableID);
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
        
        // Specify file name
        filename = filename?filename+'.xls':'excel_data.xls';
        
        // Create download link element
        downloadLink = document.createElement("a");
        
        document.body.appendChild(downloadLink);
        
        if(navigator.msSaveOrOpenBlob){
            var blob = new Blob(['\ufeff', tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob( blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
        
            // Setting the file name
            downloadLink.download = filename;
            
            //triggering the function
            downloadLink.click();
        }
    }

    $('.export-spreadsheet').on('click', function(){
        exportTableToExcel('my_table', 'members-data');
    });
});